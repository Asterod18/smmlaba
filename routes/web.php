<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Client\ClientController@index')->name('home');

Route::get('/category/{id}','Client\ClientController@categoryPage')->name('clientCategory');
Route::get('/service/{slug}','Client\ServiceController@show')->name('clientService');

Auth::routes();

Route::post('/profile/orders/return','Client\ClientController@orderBack')->name('afterOrder');
Route::post('/profile/orders/callback','Client\ClientController@orderBackCallback')->name('updateOrder');

Route::middleware('auth')->group(function () {
    Route::get('/profile/order/create','Client\ClientController@orderCreate')->name('orderCreate');


	Route::get('/profile/orders','Client\ClientController@orders')->name('orders');
	Route::get('/profile/orders/{id}','Client\ClientController@orderDetails')->name('orderDetails');
	Route::post('/profile/orders/{id}','Client\ClientController@reOrder')->name('reorder');

	Route::get('/profile/orders/{id}/service/{service_id}','Client\ClientController@orderItemDetails')->name('orderItemDetails');
	Route::get('/support','Client\TicketController@index')->name('support');
	Route::post('/support','Client\TicketController@store')->name('ticketStore');
	Route::post('/support/{id}','Client\CommentController@store')->name('commentAdd');
	Route::get('/support/add','Client\TicketController@create')->name('ticketAdd');
	Route::get('/support/{id}','Client\TicketController@show')->name('ticketShow');
	Route::post('/service/{slug}','Client\CartController@addItem')->name('addItem');
    Route::delete('/services','Client\CartController@removeItem')->name('removeItem');
    Route::get('/profile','Client\ClientController@profile')->name('profile');
	Route::get('/profile/cart/','Client\CartController@index')->name('cart');
});
Route::middleware('admin')->group(function () {
  Route::prefix('dashboard')->group(function () {
    Route::get('/','AdminDashboardController@index')->name('dashboard');
    Route::resource('services','ServiceController');
    Route::resource('category','CategoryController');
    Route::resource('user','UserController');
    Route::get('/support/','TicketController@index')->name('supportDash');
    Route::get('/support/{id}','TicketController@show')->name('ticketShowDash');
    Route::post('/support/{id}','TicketController@store')->name('CommentAddDash');

	  Route::get('/orders/','OrderController@index')->name('ordersDash');
	  Route::get('/orders/{id}','OrderController@show')->name('OrdersShowDash');
  });
});
