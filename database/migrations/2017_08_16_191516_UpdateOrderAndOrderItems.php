<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrderAndOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
	    Schema::table('orders', function (Blueprint $table) {
		    $table->dropColumn( 'smm_id');
	    });
	    Schema::table('orders_services', function (Blueprint $table) {
		    $table->string( 'smm_id')->nullable()->default(null);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
	    Schema::table('orders', function (Blueprint $table) {
		    $table->string( 'smm_id')->nullable()->default(null);
	    });
	    Schema::table('orders_services', function (Blueprint $table) {
		    $table->dropColumn( 'smm_id');
	    });
    }
}
