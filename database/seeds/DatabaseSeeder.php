<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
	    $this->call(ServicesTableSeeder::class);
        factory(App\Service::class,55)->create();
        $this->call(TStatusesTableSeeder::class);
        $this->call(OrderStatusTableSeeder::class);
    }
}
