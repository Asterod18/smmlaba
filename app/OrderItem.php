<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'orders_services';

    public $timestamps = false;

    public static function createFromCartItem(CartItem $cartItem)
    {
        $item = new OrderItem();
        $item->url = $cartItem->url;
        $item->amount = $cartItem->amount;
        $item->price = $cartItem->price;
        $item->service_id = $cartItem->service_id;
        return $item;
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function orders()
    {
        return $this->belongsTo('App\Order');
    }
}
