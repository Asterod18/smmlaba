@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Редактировать</div>

                <div class="panel-body">

                    {{ Form::model('App\Service', ['method'=>'POST', 'files'=>true, 'route' => ['services.store']]) }}
                    <div class="form-group">
                        {{Form::label('title', 'Название')}}
                        {{Form::text('title',null,['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('price', 'Цена за 100')}}
                        {{Form::text('price',null,['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('key', 'Ключ')}}
                        {{Form::text('key',null,['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('category_id', 'Категория')}}
                        {{Form::select('category_id', $categories, null, ['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('description', 'Описание')}}
                        {{Form::textarea('description', null, ['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('image', 'Изображение')}}
                        {{Form::file('image',['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('min', 'Минимальное количество')}}
                        {{Form::number('min',null,['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('max', 'Максимальное количество')}}
                        {{Form::number('max',null,['class' => 'form-control'])}}
                    </div>
                    {{ Form::submit('Сохранить', ['class' => 'btn btn-default btn-xs']) }}
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var editor_config = {
            path_absolute: "/",
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            relative_urls: false,
            file_browser_callback: function (field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file: cmsURL,
                    title: 'Filemanager',
                    width: x * 0.8,
                    height: y * 0.8,
                    resizable: "yes",
                    close_previous: "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>
@endsection