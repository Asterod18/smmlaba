@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Редактировать</div>

                <div class="panel-body">

                    {{ Form::model($category, ['method'=>'PATCH', 'files'=> true, 'route' => ['category.update', $category->id]]) }}
                    <div class="form-group">
                        {{Form::label('title', 'Название')}}
                        {{Form::text('title',null,['class' => 'form-control'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('description', 'Описание')}}
                        {{Form::textarea('description', null, ['class' => 'form-control'])}}
                    </div>
                    @if ($category->parent_id == null)
						<?php $selected = 0;?>
                    @else
						<?php $selected = $category->parent_id;?>
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{Form::label('parent_id', 'Категория')}}
                                {{Form::select('parent_id', $categories, $selected, ['class' => 'form-control'])}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{Form::label('image', 'Изображение')}}
                                {{Form::file('image',['class'=>'form-control'])}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('rule', 'Валидатор')}}
                        {{Form::text('rule',null,['class'=>'form-control'])}}
                    </div>
                    {{ Form::submit('Сохранить', ['class' => 'btn btn-default btn-xs']) }}
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var editor_config = {
            path_absolute: "/",
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            relative_urls: false,
            file_browser_callback: function (field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file: cmsURL,
                    title: 'Filemanager',
                    width: x * 0.8,
                    height: y * 0.8,
                    resizable: "yes",
                    close_previous: "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>
@endsection 