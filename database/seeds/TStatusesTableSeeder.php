<?php

use Illuminate\Database\Seeder;

class TStatusesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//
		DB::table('ticket_statuses')->insert([
			'title'=>'open'
		]);
		DB::table('ticket_statuses')->insert([
			'title'=>'closed'
		]);
		DB::table('ticket_statuses')->insert([
			'title'=>'answered'
		]);

	}
}
