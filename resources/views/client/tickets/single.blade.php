@extends('layouts.app')

@section('meta')
    {{config('app.name', 'Laravel')}} - Тикет {{$ticket->subject}}
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2>{{$ticket->subject}}</h2>
            <ul class="tickets">
                @foreach($ticket->Comments as $comment)
                    <li @if($comment->user_id != Auth::user()->id)
                            class="admin_answer"
                        @endif
                        >
                        <div>
                            <div class="comment_info">
                                <span class="comment_author">{{$comment->User->name}}</span>
                                <span class="comment_time">{{$comment->created_at}}</span>
                            </div>
                            <div class="comment_text">
                                {!! $comment->content !!}
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            {{Form::open(['method'=>'POST', 'route' => ['commentAdd', $ticket->id] , 'class'=> ''])}}
            <div class="form-group ">
                {{Form::label('content', 'Сообщение', ['class'=>'col-md-2'])}}
                {{Form::textarea('content',null,['class' => 'form-control'])}}
            </div>
            {{ Form::submit('Сохранить', ['class' => 'btn btn-default']) }}
            {{Form::close()}}
            </div>
        </div>
    </div>
@endsection



@section('scripts')
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection