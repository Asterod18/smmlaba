<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $table = 'carts_services';

    public $timestamps = false;

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }
}
