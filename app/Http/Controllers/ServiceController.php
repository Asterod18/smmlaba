<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Category;
use Illuminate\Support\Facades\Redirect;
use Packages\SmmlabaApi\Client as ApiClient;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $services = Service::paginate(10);
      return view('dashboard.services.index',['services'=>$services]);
    }


    public function viewAvailable()
    {
        $api = new ApiClient();
        print_r($api->services());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::Tree();
        return view('dashboard.services.add',['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $service = new Service($request->except('image'));
	    if($request->file('image')){
		    $image = $request->file('image');

		    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

		    $destinationPath = public_path('/images/services');

		    $image->move($destinationPath, $input['imagename']);
		    $service->images = $input['imagename'];
	    }

        $service->save();
        return Redirect::route('services.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $item = Service::find($id);

        return view('dashboard.services.show',['service'=>$item]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);
        $categories = Category::Tree();
        return view('dashboard.services.edit', ['service'=>$service, 'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::findOrFail($id);

	    if($request->file('image')){
		    $image = $request->file('image');

		    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

		    $destinationPath = public_path('/images/services');

		    $image->move($destinationPath, $input['imagename']);
		    $service->images = $input['imagename'];
	    }
	    $service->update($request->except('image'));
        return Redirect::route('services.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Service::findOrFail($id);
        $item->delete();
        return Redirect::route('services.index');
    }
}
