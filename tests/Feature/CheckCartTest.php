<?php

namespace Tests\Feature;

use App\Service;
use App\Cart;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CheckCartTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        Auth::loginUsingId(1);
        $service = Service::findOrFail(1);
        $cart = Auth::user()->getCart();
        $cart->add($service,10,'test_url');
        $result_1 = count($cart->items) == 1 &&
            $cart->items->first()->price==1500 &&
            $cart->price == 1500 &&
            $cart->count == 1;
        $cart->add($service,2,'test_url_2');
        $result_2 = count($cart->items()->get()) == 2 &&
            $cart->price == 1800 &&
            $cart->count == 2;
        $this->assertTrue($result_1 && $result_2);
    }
}
