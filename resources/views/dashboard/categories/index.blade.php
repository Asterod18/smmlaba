@extends('layouts.dashboard') @section('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">Категории</div>

      <div class="panel-body">

        @if (session('message'))
          <div class="alert alert-success">
            {{ session('message') }}
          </div>
        @endif
        <ul class="treeMenu">
          @foreach($categories as $category)
          <li><i class="fa fa-folder"></i> <a href="{{route('category.show',['id'=>$category->id])}}">{{$category->title}}</a>
            <ul style="display: none;">
              @foreach($category->children as $child)
              <li><a href="{{route('category.show',['id'=>$child->id])}}"><i class="fa fa-file-o"></i>{{$child->title}}</a></li>
              @endforeach
            </ul>
          </li>
          @endforeach
          <li><a href="{{route('category.create')}}"><i class="fa fa-plus"></i> Добавить новую</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
@endsection
