<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('services')->insert([
                    'title' => 'ВКонтакте - Вступившие\Подписчики в паблик\группу',
                    'price' => 150,
                    'key' => 'vkpreconom',
                    'description'=>'<p>Вступившие - Живые Офферы. <strong>Скорость 40-100 офферов в сутки.&nbsp;</strong><strong>Группа должна быть открыта.&nbsp;</strong><strong>&nbsp;Кол-во заблокированных аккаунтов (собачек) может быть высоким.</strong></p>',
                    'category_id'=> 2
                ]);

    }
}
