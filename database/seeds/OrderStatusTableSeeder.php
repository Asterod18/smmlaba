<?php

use Illuminate\Database\Seeder;

class OrderStatusTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//
		DB::table('status_order')->insert([
			'id'=>1,
			'title'=>'new',
			'description'=>'Waiting for service answer',
		]);
		DB::table('status_order')->insert([
		    'id'=>2,
			'title'=>'pending',
			'description'=>'Payment is pending',
		]);
        DB::table('status_order')->insert([
            'id'=>3,
            'title'=>'success',
            'description'=>'Payment is success',
        ]);
        DB::table('status_order')->insert([
            'id'=>4,
            'title'=>'failure',
            'description'=>'Payment is failed',
        ]);
	}
}
