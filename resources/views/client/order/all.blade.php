@extends('layouts.app')

@section('meta')
    {{config('app.name', 'Laravel')}} - История заказов
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <h2>Ваши заказы</h2>
            @if(count($orders) > 0)
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Количество заказанных сервисов</th>
                    <th>Цена</th>
                    <th>Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->amount}}</td>
                        <td>{{$order->price}} <span class="currency">&#8372;</span></td>
                        <td>{{$order->status->title}}</td>
                        <td>
                            <a href="{{route('orderDetails',['id'=>$order->id])}}" class="btn btn-info btn-xs">Подробнее</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @else
                <p>У вас еще нет заказов, <a href="{{route('home')}}">вернуться на главную</a></p>
            @endif
        </div>
    </div>
@endsection
