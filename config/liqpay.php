<?php
return [
	'public_key'          => env('LIQPAY_PUBLIC_KEY', 'test'),
	'private_key'         => env('LIQPAY_PRIVATE_KEY', 'test'),
	'description_payment' => env('APP_NAME').' - оплата сервиса',
	'sandbox' => env('APP_DEBUG', true),
];

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/17/2017
 * Time: 6:09 PM
 */