@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Категория {{$category->id}}</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h5>Название</h5>
                            <h3>{{$category->title}}</h3>
                        </div>
                        <div class="col-md-4">
                            @if($category->images)
                                <h5>Иконка</h5>
                                <img src="/images/{{$category->images}}" class="img-responsive" style="width:40px;">
                            @endif
                        </div>
                    </div>
                    <hr>
                    @if($category->parent)
                        <h5>Категория</h5>
                        <h3>{{$category->parent->title}}</h3>
                        <hr>
                    @else
                        <h5>Валидатор</h5>
                        <h3>{{$category->rule}}</h3>
                        <hr>
                    @endif
                    <h5>Описание</h5>
                    <div class="description">
                        {!!$category->description!!}
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="{{route('category.edit',['/id'=>$category->id])}}"
                       class="btn btn-warning">Редактировать</a>
                    {{ Form::open(['method' => 'DELETE', 'route' => ['category.destroy', $category->id]]) }}
                    {{ Form::hidden('id', $category->id) }}
                    {{ Form::submit('Удалить', ['class' => 'btn btn-danger']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
