<?php
namespace Packages\SmmlabaApi;


class Client
{
    const apiUrl = 'https://smmlaba.com/vkapi/v1/';
    private $curl;

    public $username;
    public $apikey;

    public function __construct()
    {
        $this->curlInit();
        $this->username = config('smmlaba.username');
        $this->apikey = config('smmlaba.key');
    }

    public function __destruct()
    {
        $this->curlClose();
    }

    private function curlInit()
    {
        $this->curl = curl_init();
        //curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_USERAGENT, 'Curl/Api');
    }

    private function curlClose()
    {
        curl_close($this->curl);
    }

    private function curlPost($url, array $postData = array())
    {

        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_POST, count($postData));
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $this->getPostString($postData));
        $curlRes = curl_exec($this->curl);

        if (curl_error($this->curl)) {
            return false;
        } else {
            return $curlRes;
        }
    }

    private function getPostString(array $postData)
    {
        $post_string = '';
        foreach ($postData as $key => $value) {
            $post_string .= $key . '=' . $value . '&';
        }
        return rtrim($post_string, '&');
    }

    private function getInitArray()
    {
        return array('username' => $this->username, 'apikey' => $this->apikey);
    }

    private function postDecode($str)
    {
        if (!$str) return false;
        $result = json_decode($str, true);
        if (json_last_error() == JSON_ERROR_NONE) {
            return $result;
        } else {
            return false;
        }
    }

    public function balance()
    {
        $data = $this->getInitArray();
        $data['action'] = 'balance';
        return $this->postDecode($this->curlPost($this::apiUrl, $data), true);
    }

    public function check($orderid)
    {
        $data = $this->getInitArray();
        $data['action'] = 'check';
        $data['orderid'] = $orderid;
        return $this->postDecode($this->curlPost($this::apiUrl, $data), true);
    }

    public function add($service, $url, $count)
    {
        $data = $this->getInitArray();
        $data['action'] = 'add';
        $data['service'] = $service;
        $data['url'] = $url;
        $data['count'] = $count;
        return $this->postDecode($this->curlPost($this::apiUrl, $data), true);
    }

    public function services($service = '')
    {
        $data = $this->getInitArray();
        $data['action'] = 'services';
        if ($service) $data['service'] = $service;
        return $this->postDecode($this->curlPost($this::apiUrl, $data), true);
    }
}
