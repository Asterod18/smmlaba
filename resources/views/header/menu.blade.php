<div id="scroll" style="">


    <a id="touch-menu" class="mobile-menu" href="#"><i class="icon-reorder"></i>Меню</a>

    <nav>

        <div id="menu" style="display:block">
            <ul class="menu">
                @foreach($categories as $category)
                    <li class="img">
                        <a href="{{route('clientCategory',[ 'id' => $category->id])}}">
                        <img src="/images/{{$category->images}}" alt="{{$category->title}}" title="{{$category->title}}"></a>
                        <div style="">
                        @if($category->children())
                            <ul class="sub-menu">


                                @foreach($category->children as $children)
                                    <li>
                                        <a href="{{route('clientCategory',['id'=>$children->id])}}" >{{$children->title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                        </div>
                    </li>
                @endforeach


            </ul>
        </div>
    </nav>


</div>