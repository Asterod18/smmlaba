<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	//
	protected $fillable = [
		'title',
		'description',
		'parent_id',
		'images',
		'rule'
	];

	public function parent()
	{
		return $this->belongsTo('App\Category', 'parent_id');
	}

	public function children()
	{
		return $this->hasMany('App\Category', 'parent_id');
	}

	public function Services()
	{
		return $this->hasMany('App\Service', 'category_id');
	}

	public static function Tree()
	{
		$categories = self::root()->get();
		$massive = [];
		foreach ($categories as $category) {
			if (count($category->children) == 0):
				$massive[ $category->title ] = [];
			else:
				foreach ($category->children as $children) {
					$massive[ $category->title ][ $children->id ] = $children->title;
				}
			endif;
		}

		return $massive;
	}

	public static function Parents()
	{
		$categories = self::root()->get();
		$massive = [];
		foreach ($categories as $category) {
			$massive[ $category->id ] = $category->title;
		}

		return $massive;
	}

	public static function scopeRoot($query)
	{
		return $query->where('parent_id', null);
	}

}
