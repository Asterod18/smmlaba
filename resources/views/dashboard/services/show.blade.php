@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Сервис {{$service->id}}</div>

                <div class="panel-body">
                    <h5>Название</h5>
                    <h3>{{$service->title}}</h3>
                    <hr>
                    <h5>Изображение</h5>
                    <img src="/images/services/{{$service->images}}">
                    <h5>Цена за 100</h5>
                    <h3>{{$service->price}}</h3>
                    <hr>
                    <h5>Ключ</h5>
                    <h3>{{$service->key}}</h3>
                    <hr>
                    <h5>Категория</h5>
                    <h3>{{$service->Category->title}}</h3>
                    <hr>
                    <h5>Описание</h5>
                    <div class="description">
                        {!!$service->description!!}
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="{{route('services.edit',['id'=>$service->id])}}" class="btn btn-warning">Edit</a>
                    {{ Form::open(['method' => 'DELETE', 'route' => ['services.destroy', $service->id]]) }}
                    {{ Form::hidden('id', $service->id) }}
                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
