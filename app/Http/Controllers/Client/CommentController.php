<?php

namespace App\Http\Controllers\Client;

use App\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;

class CommentController extends Controller
{
	//
	public function store(Request $request, $id)
	{
		$ticket = Ticket::findOrFail($id);
		$ticket->comments()->create([
			'user_id' => Auth::User()->id,
			'content' => $request->input('content'),
		]);

		return Redirect::route('ticketShow', $ticket->id);
	}
}
