<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Packages\SmmlabaApi\Client as ApiClient;

class Order extends Model
{

	protected $table = 'orders';

	public $timestamps = true;

	public static function createFromCart(Cart $cart)
	{
		$order = new Order();
		$order->user_id = $cart->user_id;
		$order->price = $cart->price;
		$order->amount = $cart->count;
		$order->save();
		$cart->items->map(function ($item) use ($order) {
			$orderItem = OrderItem::createFromCartItem($item);
			$orderItem->order_id = $order->id;
			$orderItem->save();

			return $orderItem;
		});
		$cart->delete();

		return $order;
	}

	public function reorder()
	{
		$new = $this->replicate();
		$new->status_order_id = 1;
		$new->save();
		$this->items->map(function ($item) use ($new) {
			$item->order_id = $new->id;
			$item->save();
		});

		$this->delete();

		return $new;
	}

	public function user()
	{
		$this->belongsTo('App\User');
	}

	public function log()
	{
		$this->belongsToMany('App\OrderStatus', 'log_order', 'order_id', 'status_id')->withPivot(['jump_time']);
	}

	public function status()
	{
		return $this->belongsTo('App\OrderStatus', 'status_order_id');
	}

	public function items()
	{
		return $this->hasMany('App\OrderItem');
	}

	public function sendSMM(){
		$this->items->map(function(OrderItem $item){
			//TODO integrate smm api

			$api = new ApiClient();
			$details = $api->add(
				$item->service->key,
				$item->url,
				$item->amount
			);
			$item->smm_id = $details['message']['orderid'];
			$item->save();
			return $item;
		});

	}
}
