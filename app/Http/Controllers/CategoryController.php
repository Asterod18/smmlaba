<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Redirect;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::root()->get();
        return view('dashboard.categories.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::Parents();
        $categories[0] = 'Новая категория';
        return view('dashboard.categories.add', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();

	    $category->title = $request->input('title');
	    $category->description = $request->input('description');

	    if ($request->input('parent_id') == 0) {
		    $category->parent_id = null;
		    $category->rule = $request->input('rule');
		    if($request->file('image')){
			    $image = $request->file('image');

			    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

			    $destinationPath = public_path('/images');

			    $image->move($destinationPath, $input['imagename']);
			    $category->images = $input['imagename'];
		    }
	    }else{
		    $category->parent_id = $request->input('parent_id');
	    }

	    $category->save();
	    return redirect()->route('category.index')->with('message','Категория добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item = Category::findOrFail($id);

        return view('dashboard.categories.show', ['category' => $item]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::findOrFail($id);

        $categories = Category::Parents();
        $categories[0] = 'Новая категория';

        return view('dashboard.categories.edit', ['category' => $category, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $category = Category::findOrFail($id);

        $category->title = $request->input('title');
	    $category->description = $request->input('description');

	    if ($request->input('parent_id') == 0) {
		    $category->parent_id = null;
		    $category->rule = $request->input('rule');

		    if($request->file('image')){

			    $image = $request->file('image');

			    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

			    $destinationPath = public_path('/images');

			    $image->move($destinationPath, $input['imagename']);
			    $category->images = $input['imagename'];

		    }
	    }
	    $category->update();

        return redirect()->route('category.index')->with('message','Категория обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
	    $category = Category::findOrFail($id);
		$category->delete();

	    return redirect()->route('category.index')->with('message','Категория удалена');
    }
}
