@extends('layouts.app')

@section('meta')
    {{config('app.name', 'Laravel')}} - Заказ {{$order->id}}
@endsection


@section('content')

    <div class="container">
        <div class="row">
            <h2>Заказ № {{$order->id}}</h2>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Сервис</th>
                    <th>Количество</th>
                    <th>Ссылка</th>
                    <th>Цена</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->items as $order_item)
                    <tr>
                        <td>{{$order_item->id}}</td>
                        <td>
                            @if($order->status_order_id == 3)
                                <a href="{{route('orderItemDetails',['id'=>$order->id,'service_id'=>$order_item->id])}}">{{$order_item->service->title}}</a>
                            @else
                                {{$order_item->service->title}}
                            @endif
                        </td>
                        <td>{{$order_item->amount}}</td>
                        <td>{{$order_item->url}}</td>
                        <td>{{$order_item->price}} <span class="currency">&#8372;</span></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="">
                <p> Суммарно : <span>{{$order->price}}</span><span class="currency">&#8372;</span></p>
            </div>
            @if($order->status->id == 1)
                {!! $button !!}
            @elseif($order->status->id == 4)
                {{ Form::open(['method' => 'POST', 'route' => ['reorder',$order->id]]) }}
                {{ Form::submit('Reorder', ['class' => 'btn btn-danger btn-xs']) }}
                {{ Form::close() }}
            @endif
        </div>
    </div>
@endsection
