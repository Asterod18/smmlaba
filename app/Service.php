<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Service extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    //
    protected $fillable = [
        'title',
        'price',
        'key',
        'category_id',
        'description',
	    'images',
	    'min',
	    'max'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'service_id');
    }

    public function getPrice($amount)
    {
        return $this->price/100 * $amount;
    }

    public function items()
    {
        return $this->hasMany('App\CartItem');
    }

    public function rootCategoryRule(){
    	$category_rule = Category::find($this->category->id)->parent->rule;
    	return $category_rule;


    }

}
