<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('status_order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title'); // оплачен, исполняется, ... , исполнен
            $table->text('description'); // текст для всплывающей подсказки
            $table->boolean('using')->default(true); // для аднинистратора
        });

        Schema::table('orders',function(Blueprint $table){
            $table->foreign('status_order_id')->references('id')->on('status_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders',function(Blueprint $table){
            $table->dropForeign(['status_order_id']);
        });
        //
        Schema::dropIfExists('status_order');
    }
}
