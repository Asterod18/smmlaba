<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinMaxrules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
	    Schema::table('services', function (Blueprint $table) {
		    $table->integer( 'min');
		    $table->integer( 'max');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

	    Schema::table('services', function (Blueprint $table) {
		    $table->dropColumn( 'min');
		    $table->dropColumn( 'max');
	    });

    }
}
