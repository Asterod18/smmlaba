@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="404"> 404 </div>
            <p>Запрашиваемая страница не найдена.</p>
            <p><a href="{{route('home')}}">Домой</a></p>
        </div>
    </div>
</div>
@endsection