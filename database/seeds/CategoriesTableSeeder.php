<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        DB::table('categories')->insert([
            'id' => 1,
            'title' => 'Вконтакте',
            'description' => '<h1>Раскрутка ВКонтакте ‒ комплексное решение с многообразием вариантов</h1>',
            'parent_id' => null,
        ]);
        DB::table('categories')->insert([
            'id' => 2,
            'title' => 'Накрутка подписчиков в группу Вконтакте',
            'description' => '<h1>Накрутить подписчиков в группу ВК с помощью сервиса SMMLaba</h1>',
            'parent_id' => 1,
        ]);
    }
}
