$(document).ready(function(){


  $(".submenu > a").click(function(e) {
    e.preventDefault();
    var $li = $(this).parent("li");
    var $ul = $(this).next("ul");

    if($li.hasClass("open")) {
      $ul.slideUp(350);
      $li.removeClass("open");
    } else {
      $(".nav > li > ul").slideUp(350);
      $(".nav > li").removeClass("open");
      $ul.slideDown(350);
      $li.addClass("open");
    }
  });
  $('#link_expandAllButton').on('click', function (e) {
            $('ul.treeMenu i.fa-folder').removeClass('fa fa-folder').addClass('fa fa-folder-open');
            $('ul.treeMenu ul').slideDown('slow');
            return false;
        });

        $('#link_collapseAllButton').on('click', function (e) {
            $('ul.treeMenu i.fa-folder-open').removeClass('fa fa-folder-open').addClass('fa fa-folder');
            $('ul.treeMenu ul').slideUp('slow');
            return false;
        });


        $('ul.treeMenu li').on('click', function (e) {
            if ($(this).has("ul").length) {
                if ($(this).find('ul:first').is(":visible")) {
                    $(this).find('i:first').removeClass('fa fa-folder-open').addClass('fa fa-folder');
                    $(this).find('ul:first').slideUp();
                    e.stopPropagation();
                } else {
                    $(this).find('ul:first').slideDown('slow');
                    $(this).find('i:first').removeClass('fa fa-folder').addClass('fa fa-folder-open');
                    e.stopPropagation();
                }
            } else {
                e.stopPropagation();
            }
        });

        //COMENNT OUT NEXT TWO LINES TO DEFAULT TO COLLAPSED
        $('ul.treeMenu i.fa-folder').removeClass('fa fa-folder').addClass('fa fa-folder-open');
        $('ul.treeMenu ul').slideDown('slow');
});
