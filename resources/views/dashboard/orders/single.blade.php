@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row">
            <h2>Заказ {{$order->id}}</h2>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>SMM id</th>
                    <th>Сервис</th>
                    <th>Ссылка</th>
                    <th>Количество</th>
                    <th>Цена</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->items as $orderItem)
                    <tr>
                        <td>{{$orderItem->id}}</td>

                        <td>
                            @if($orderItem->smm_id)
                                {{$orderItem->smm_id}}
                            @else
                                Не оплачен
                            @endif
                        </td>
                        <td>{{$orderItem->service->title}}</td>
                        <td>{{$orderItem->url}}</td>
                        <td>{{$orderItem->amount}}</td>
                        <td>{{$orderItem->price}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


