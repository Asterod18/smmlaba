<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class TicketController extends Controller
{
    //
	public function index()
	{
		$tickets = Ticket::paginate(10);
		return view('dashboard.tickets.all',['tickets'=>$tickets]);
	}


	public function store(Request $request, $id)
	{

		$ticket = Ticket::findOrFail($id);
		$ticket->status_id = $request->input('status_id');
		$ticket->save();
		if($request->input('content')){
			$ticket->comments()->create([
				'user_id' => Auth::User()->id,
				'content' => $request->input('content')
			]);
		}

		return Redirect::route('ticketShowDash', $ticket->id);
	}

	public function show($id)
	{
		$ticket = Ticket::findOrFail($id);
		return view('dashboard.tickets.single',['ticket'=>$ticket]);
	}
}
