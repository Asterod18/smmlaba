@extends('layouts.app')
@section('meta')
    {{config('app.name', 'Laravel')}} - Все тикеты
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <h2>Сообщения</h2>
            <a href="{{route('ticketAdd')}}" class="btn btn-primary">Создать тикет</a>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Тема</th>
                    <th>Статус</th>
                    <th>Дата добавления</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tickets as $ticket)
                    <tr>
                        <td>{{$ticket->id}}</td>
                        <td><a href="{{route('ticketShow',['id'=>$ticket->id])}}">{{$ticket->subject}}</a></td>
                        <td>{{$ticket->Status->title}}</td>
                        <td>{{$ticket->created_at}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
