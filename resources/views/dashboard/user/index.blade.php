@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Пользователи</div>

            <div class="panel-body">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Идентификатор</th>
                      <th>Имя</th>
                      <th>Почтовый ящик</th>
                      <th>Зарегистрирован</th>
                      <th>Действие</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                      <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->created_at}}</td>
                        {{--  <td>
                          <a href="{{route('users.edit',['id'=>$user->id])}}" class="btn btn-warning btn-xs">Edit</a>
                          {{ Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id]]) }}
                              {{ Form::hidden('id', $user->id) }}
                              {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
                          {{ Form::close() }}
                        </td>
                        --}}
                      </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
