<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\UserDetail;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = User::create([
            'name' => 'admin',
            'email' => 'asterod18@gmail.com',
            'password' => bcrypt('admin'),
        ]);
        $user_gropp = new UserDetail(['user_group_id' => 1]);
        $user->UserDetails()->save($user_gropp);

        $user2 = User::create([
            'name' => 'konstantin',
            'email' => 'konstantin.b.ysbm@gmail.com',
            'password' => bcrypt('asterod18'),
        ]);
        $user_gropp2 = new UserDetail(['user_group_id' => 2]);
        $user2->UserDetails()->save($user_gropp2);
    }
}
