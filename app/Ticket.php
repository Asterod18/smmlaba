<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	//
	protected $fillable = [
		'user_id',
		'subject',
		'status_id',
	];

	public function comments()
	{
		return $this->hasMany('App\Comment');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function status()
	{
		return $this->belongsTo('App\TicketStatus');
	}
}
