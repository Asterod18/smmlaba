<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	//
	protected $fillable = [
		'user_id',
		'content',
	];

	public function Ticket()
	{
		return $this->belongsTo('App\Comment');
	}

	public function User()
	{
		return $this->belongsTo('App\User');
	}
}
