@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row">
            <h2>Заказы</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Количество сервисов</th>
                        <th>Стоимость</th>
                        <th>Статус</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->amount}}</td>
                        <td>{{$order->price}}</td>
                        <td>{{$order->status->title}}</td>
                        <td><a href="{{route('OrdersShowDash',['id'=>$order->id])}}" class="btn btn-xs btn-info">Подробнее</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $orders->links() }}
        </div>
    </div>
@endsection
