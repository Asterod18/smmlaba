<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\UserDetail;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders()
    {
        return $this->hasMany('App\Order','user_id');
    }

    public function userDetails()
    {
        return $this->hasOne('App\UserDetail');
    }

    public function isAdmin()
    {
        return ($this->UserDetails->user_group_id == 1); // this looks for an admin column in your users table
    }

    public function Tickets()
    {
    	return $this->hasMany('App\Ticket');
    }

    public function cart()
    {
    	return $this->hasOne('App\Cart');
    }

    public function getCart()
    {
    	if ($this->cart == null) {
            $cart = new Cart();
            $cart->user_id = $this->id;
            $cart->save(); // это 3 раза отрабатывает

	    } else {
            $cart = $this->cart;
	    }
        return $cart;
    }
}
