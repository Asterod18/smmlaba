@extends('layouts.app')
@section('meta')
    {{config('app.name', 'Laravel')}} - Корзина
@endsection
@section('content')

    <div class="container">
        <div class="row">
            <h2>Корзина</h2>
            @if(count($cart->items) > 0)
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Сервис</th>
                    <th>Количество</th>
                    <th>Ссылка</th>
                    <th>Цена</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cart->items as $cart_item)
                    <tr>
                        <td>{{$cart_item->service->title}}</td>
                        <td>{{$cart_item->amount}}</td>
                        <td>{{$cart_item->url}}</td>
                        <td>{{$cart_item->price}}<span class="currency">&#8372;</span></td>
                        <td>
                            {{ Form::open(['method' => 'DELETE', 'route' => ['removeItem']]) }}
                            {{ Form::hidden('id', $cart_item->id) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

                <div class="">
                    <p> Суммарно : <span>{{$cart->price}}</span><span class="currency">&#8372;</span></p>
                </div>
            <a class="btn btn-success " href="{{route('orderCreate')}}">Оплатить</a>
            @else
                <p>Ваша корзина пуста, <a href="{{route('home')}}">вернуться на главную</a></p>
            @endif
        </div>
    </div>
@endsection
