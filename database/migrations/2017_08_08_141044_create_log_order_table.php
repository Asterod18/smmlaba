<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('log_order', function (Blueprint $table) {
            $table->integer('order_id')->unsigned(); // заказ из orders
            $table->foreign('order_id')->references('id')->on('orders');
            $table->timestamp('jump_time'); // дата:время изменения статуса
            $table->integer('status_id')->unsigned(); // статус из status_order
            $table->foreign('status_id')->references('id')->on('status_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('log_order');
    }
}
