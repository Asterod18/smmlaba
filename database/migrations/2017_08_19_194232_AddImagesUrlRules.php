<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesUrlRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
	    Schema::table('categories', function (Blueprint $table) {
		    $table->string( 'images')->nullable()->default(null);
	        $table->string('rule')->nullable()->default(null);
	    });
	    Schema::table('services', function (Blueprint $table) {
		    $table->string( 'images')->nullable()->default(null);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
	    Schema::table('categories', function (Blueprint $table) {
		    $table->dropColumn( 'images');
		    $table->dropColumn('rule');
	    });
	    Schema::table('services', function (Blueprint $table) {
		    $table->dropColumn( 'images');
	    });
    }
}
