<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;

class ServiceController extends Controller
{
	//
	public function show($slug)
	{
		$service = Service::where('key', $slug)->firstOrFail();

		return view('client.services.single', ['service' => $service]);
	}
}
