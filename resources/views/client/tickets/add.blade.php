@extends('layouts.app')
@section('meta')
    {{config('app.name', 'Laravel')}} - Добавить тикет
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <h3>Добавить тикет</h3>
            {{Form::open(['method'=>'POST', 'route' => ['ticketStore'] , 'class'=>'form-horizontal'])}}
            <div class="form-group">
                {{Form::label('subject','Тема', ['class'=>'col-md-2'])}}
                <div class="col-sm-10">
                    {{Form::text('subject',null,['class' => 'form-control'])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('content', 'Сообщение', ['class'=>'col-md-2'])}}
                <div class="col-sm-10">
                    {{Form::textarea('content',null,['class' => 'form-control'])}}
                </div>
            </div>
            {{ Form::submit('Сохранить', ['class' => 'btn btn-default']) }}
            {{Form::close()}}
        </div>
    </div>
@endsection

@section('scripts')
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection