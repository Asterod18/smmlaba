<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\UnauthorizedException;

class Cart extends Model
{
    protected $table = 'carts';

    public $timestamps = false;


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function items()
    {
        return $this->hasMany('App\CartItem');
    }

    public function add(Service $service, $amount, $url)
    {
        $item = new CartItem();
        $item->cart_id = $this->id;
        $item->service_id = $service->id;
        $item->url = $url;
        $item->amount = $amount;
        $item->price = $service->getPrice($amount);
        $item->save();

        $this->count += 1;
        $this->price += $service->getPrice($amount);
	    $this->save();
    }

    public function remove($id)
    {
        $item = $this->items()
            ->where('id', $id)
            ->first();
        $this->count -= 1;
        $this->price -= $item->price;
        $item->delete();
	    $this->save();
    }

    public function widget()
    {
	    if ($this->count == 0) {
		    return '';
	    }
	    if ($this->count == 1) {
		    return '('.$this->count . ' товар)';
	    } elseif ($this->count < 5){
		    return '('.$this->count . ' товара)';
        } else {
		    return '('.$this->count.' товаров)';
	    }
    }
}