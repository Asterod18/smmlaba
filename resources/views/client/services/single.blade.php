@extends('layouts.app')
@section('meta')
    {{config('app.name', 'Laravel')}} - Сервис - {{$service->title}}
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if (Session::has('message'))
                    <div class="alert alert-success">
                        {!! Session::get('message') !!}
                    </div>
                @endif
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}">Главная</a></li>
                    <li>
                        <a href="{{route('clientCategory',['id'=>$service->category->id])}}">{{$service->category->title}}</a>
                    </li>
                    <li class="active">{{$service->title}}</li>
                </ol>
                <div class="row">
                    <div class="col-md-3">
                        <img src="/images/services/{{$service->images}}" class="img-responsive">
                    </div>
                    <div class="col-md-9">
                        {{Form::open(['method'=>'POST', 'route' => ['addItem', $service->key] , 'class'=> ''])}}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{Form::label('url', 'Ссылка', ['class'=>'col-md-2'])}}
                                    {{Form::text('url',null,['class' => 'form-control'])}}

                                    @if ($errors->get('url'))
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->get('url') as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    {{Form::label('count', 'Количество', ['class'=>'col-md-2'])}}
                                    {{Form::number('count',null,['class' => 'form-control'])}}

                                    @if ($errors->get('count'))
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->get('count') as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                {{Form::label('price','Цена')}}
                                <p id="price_block" class="price">
                                    <span id="price_current" class="current-price">0</span>
                                    <span class="currency">&#8372;</span>
                                </p>
                                {{Form::hidden('price',$service->price)}}
                                {{Form::submit('Добавить в корзину', ['class' => 'btn btn-default margin-t']) }}
                                </div>
                            </div>
                        </div>
                        {{Form::close()}}

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="description_text">
                            {!! $service->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
