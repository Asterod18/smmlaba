@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row">
            <h2>{{$ticket->subject}}</h2>
            <ul>
                @foreach($ticket->Comments as $comment)
                    <li @if($comment->user_id == Auth::user()->id)
                            class="admin_answer"
                        @endif
                        >
                        <div>
                            <div class="comment_info">
                                <span class="comment_author">{{$comment->User->name}}</span>
                                <span class="comment_time">{{$comment->created_at}}</span>
                            </div>
                            <div class="comment_text">
                                {!! $comment->content !!}
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            {{Form::open(['method'=>'POST', 'route' => ['CommentAddDash', $ticket->id] , 'class'=> 'form-horizontal'])}}
            <div class="form-group">
                {{Form::label('content', 'Сообщение', ['class'=>'col-md-2'])}}
                {{Form::textarea('content',null,['class' => 'form-control'])}}
            </div>
            <div class="form-group">
                {{Form::label('status_id', 'Статус', ['class'=>'col-md-2'])}}
                {{Form::select('status_id',['1'=>'Открыт','2'=>'Закрыт','3'=>'Отвечен',],$ticket->status_id,['class' => 'form-control'])}}
            </div>
            {{ Form::submit('Сохранить', ['class' => 'btn btn-default']) }}
            {{Form::close()}}
        </div>
    </div>
@endsection



@section('scripts')
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>
@endsection