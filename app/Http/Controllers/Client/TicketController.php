<?php

namespace App\Http\Controllers\Client;

use App\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;

class TicketController extends Controller
{
    //
	public function index()
	{
		$tickets = Auth::user()->tickets;
		return view('client.tickets.all',['tickets'=>$tickets]);
	}

	public function create()
	{
		return view('client.tickets.add');
	}

	public function store(Request $request)
	{
		$ticket = new Ticket([
			'subject' => $request->input('subject'),
			'user_id' => Auth::User()->id,
			'status_id' => '1'
		]);
		$ticket->save();
		$ticket->Comments()->create([
			'user_id' => Auth::User()->id,
			'content' => $request->input('content'),
		]);
		return Redirect::route('support');
	}

	public function show($id)
	{
		$ticket = Ticket::findOrFail($id);
		return view('client.tickets.single',['ticket'=>$ticket]);
	}
}
