@extends('layouts.app')

@section('meta')
    {{config('app.name', 'Laravel')}} - Заказанный сервис
@endsection


@section('content')

    <div class="container">
        <div class="row">
            <a href="{{route('orderDetails',['id'=>$orderItem->order_id])}}">Вернуться к заказу</a>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Сервис</th>
                    <th>Количество</th>
                    <th>Ссылка</th>
                    <th>Цена</th>
                    <th>Ход выполнения</th>
                    <th>Статус</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$orderItem->id}}</td>
                        <td>{{$orderItem->service->title}}</td>
                        <td>{{$orderItem->amount}}</td>
                        <td>{{$orderItem->url}}</td>
                        <td>{{$orderItem->price}} <span class="currency">&#8372;</span> </td>
                        <td><div class="progress"> <div class="progress-bar" role="progressbar" aria-valuenow="{{(($details['count']-$details['remain'])/$details['count'])*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{(($details['count']-$details['remain'])/$details['count'])*100}}%"> {{$details['count']-$details['remain']}}/{{$details['count']}} </div> </div></td>
                        <td>{{$details['statusText']}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
