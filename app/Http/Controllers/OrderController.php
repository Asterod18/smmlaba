<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
	public function index()
	{
		$orders = Order::paginate(10);
		return view('dashboard.orders.all',['orders'=>$orders]);
	}



	public function show($id)
	{
		$order = Order::findOrFail($id);
		return view('dashboard.orders.single',['order'=>$order]);
	}
}
