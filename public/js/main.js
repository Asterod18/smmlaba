jQuery(document).ready(function () {
    if ($(window).width() < 1000) {
        $('.menu').css('display', 'none');
    }
    $('body').on('click', '#touch-menu', function () {
        $(".menu").toggle();
    });

    $('body').on('keyup', 'input#count', function () {
        $('span#price_current').html($(this).val() * $('#price').val() / 100);
    });
});