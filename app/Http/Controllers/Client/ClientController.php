<?php

namespace App\Http\Controllers\Client;

use App\Cart;
use App\Order;
use App\OrderItem;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Service;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use LiqPay;
use Packages\SmmlabaApi\Client as ApiClient;

class ClientController extends Controller
{
	//
	public function index()
	{
		$categories = Category::Tree();

		return view('welcome', ['categories' => $categories]);
	}

	public function categoryPage($id)
	{

		$category = Category::findOrFail($id);

		if ($category->parent != null) {
			$services = $category->Services;
		} else {
			$services = new Collection();
			foreach ($category->children as $child):
				$services_find = $child->Services;
				$services = $services->merge($services_find);
			endforeach;
		}


		return view('category', ['category' => $category, 'services' => $services]);
	}

	public function orderCreate(Request $request)
	{
		$cart = Auth::user()->getCart();
		$order = Order::createFromCart($cart);

		return redirect()->route('orderDetails', ['id' => $order->id]);
	}

	public function orders()
	{
		$orders = Auth::user()->orders;

		return view('client.order.all', ['orders' => $orders]);
	}

	public function orderDetails($id)
	{
		//dd(config('liqpay.public_key'));
		$order = Order::findOrFail($id);
		$data = ['order' => $order];
		if ($order->status->id == 1) {
			$liqpay = new LiqPay(config('liqpay.public_key'), config('liqpay.private_key'));
			$form =
				['action'      => 'pay',
				 'amount'      => $order->price,
				 'currency'    => 'UAH',
				 'description' => config('liqpay.description_payment', 'test'),
				 'order_id'    => (string)$order->id,
				 'language'    => 'ru',
				 'result_url'  => route('afterOrder'),
				 'server_url'  => route('updateOrder'),
				 'version'     => '3',
				];
			if (config('liqpay.sandbox', true)) {
				$form[ 'sandbox' ] = 1;
			}
			$button = $liqpay->cnb_form($form);
			$data[ 'button' ] = $button;
		}


		return view('client.order.single', $data);
	}

	public function orderItemDetails($id, $item_id)
	{
		$orderItem = OrderItem::find($item_id);

		$order = Order::find($item_id);
		$api = new ApiClient();
		$details = $api->check($orderItem->smm_id);

		return view('client.order.singleItem', ['orderItem' => $orderItem, 'order' => $order, 'details' => $details[ 'message' ]]);
	}

	public function profile()
	{
		return view('client.profile.index');
	}

	public function orderBack(Request $request)
	{
		$data = json_decode(base64_decode($request->input('data')));
		$sign = new LiqPay(config('liqpay.public_key'), config('liqpay.private_key'));
		if ($sign->str_to_sign(config('liqpay.private_key') . $request->input('data') . config('liqpay.private_key')) == $request->input('signature')):
			switch ($data->status) {
				case "failure":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 4;
					$order->save();
					$viewData[ 'success' ] = false;
					break;
				case "error":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 4;
					$order->save();
					$viewData[ 'success' ] = false;
					break;
				case "reversed":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 4;
					$order->save();
					$viewData[ 'success' ] = false;
					break;
				case "success":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 3;
					$order->save();
					$order->sendSMM();
					$viewData[ 'success' ] = true;
					break;
				case "sandbox":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 3;
					$order->save();
					//$order->sendSMM();
					$viewData[ 'success' ] = true;
					break;
				default :
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 2;
					$order->save();
					$viewData[ 'success' ] = true;
					break;
			}

			$message = $viewData[ 'success' ];
			Session::flash('message', $message);

			return redirect(route('profile'));
		endif;
	}

	public function orderBackCallback(Request $request)
	{
		$data = json_decode(base64_decode($request->input('data')));
		$sign = new LiqPay(config('liqpay.public_key'), config('liqpay.private_key'));
		if ($sign->str_to_sign(config('liqpay.private_key') . $request->input('data') . config('liqpay.private_key')) == $request->input('signature')):
			switch ($data->status) {
				case "failure":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 4;
					$order->save();
					$viewData[ 'success' ] = false;
					break;
				case "error":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 4;
					$order->save();
					$viewData[ 'success' ] = false;
					break;
				case "reversed":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 4;
					$order->save();
					$viewData[ 'success' ] = false;
					break;
				case "success":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 3;
					$order->save();
					$order->sendSMM();
					$viewData[ 'success' ] = true;
					break;
				case "sandbox":
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 3;
					$order->save();
					$order->sendSMM();
					$viewData[ 'success' ] = true;
					break;
				default :
					$order = Order::findOrFail($data->order_id);
					$order->status_order_id = 2;
					$order->save();
					$viewData[ 'success' ] = true;
					break;
			}
		endif;

	}

	public function reOrder(Request $request, $id)
	{
		$order = Order::findOrFail($id);
		$new = $order->reorder();

		return redirect(route('orderDetails', ['id' => $new->id]));
	}
}
