<?php

namespace App\Http\Controllers\Client;

use App\Order;
use App\Service;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Redirect;
use LiqPay;

class CartController extends Controller
{
	protected $user;

	//
	public function index()
	{
		$cart = Auth::user()->getCart();

		return view('client.cart.index', ['cart' => $cart]);

	}

	public function addItem(Request $request, $slug)
	{

		$service = Service::where('key', $slug)->firstOrFail();
		$string_val= str_replace('/','\/',$service->rootCategoryRule());
		$messages = [
			'required'    => 'Поле не должно быть пустым',
			'between' => 'Количество должно быть от :min до :max.',
			'regex' => 'Ссылка должна содержать '.$service->rootCategoryRule(),
			'numeric' => 'Должно быть числом'
		];
		$validator = Validator::make($request->all(), [
			'url' => 'required|regex:/'.$string_val.'/',
			'count' => 'required|numeric|between:'.$service->min.','.$service->max,
		],$messages);
		if ($validator->fails()) {
			return redirect(route('clientService',['slug'=>$service->key]))
				->withErrors($validator)
				->withInput();
		}

		$cart = Auth::user()->getCart();
		$cart->add($service, $request->input('count'), $request->input('url'));
		$message = 'Товар <a href="' . route('clientService', ['id' => $service->id]) . '">' . $service->title . '</a> добавлен в корзину покупок!';
		Session::flash('message', $message);

		return Redirect::back();
	}

	public function removeItem(Request $request)
	{
		$id = $request->input('id');
		$cart = Auth::user()->getCart();
		$cart->remove($id);
		$message = 'Товар удален из корзины покупок!';
		Session::flash('message', $message);

		return Redirect::back();
	}

}
