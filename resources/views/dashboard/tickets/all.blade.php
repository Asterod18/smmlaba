@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row">
            <h2>Сообщения</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Тема</th>
                        <th>Статус</th>
                        <th>Дата добавления</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($tickets as $ticket)
                    <tr>
                        <td>{{$ticket->id}}</td>
                        <td><a href="{{route('ticketShowDash',['id'=>$ticket->id])}}">{{$ticket->subject}}</a></td>
                        <td>{{$ticket->Status->title}}</td>
                        <td>{{$ticket->created_at}}
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $tickets->links() }}
        </div>
    </div>
@endsection
