<?php

use Illuminate\Database\Seeder;

class TicketsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//
		DB::table('tickets')->insert([
			'user_id'=>2,
			'subject' => 'test Tickets',
			'status_id'=>1
		]);
		DB::table('tickets')->insert([
			'user_id'=>2,
			'subject' => 'test Closed Tickets',
			'status_id'=>2
		]);

	}
}
