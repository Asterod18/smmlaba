@extends('layouts.app')
@section('meta')
    {{config('app.name', 'Laravel')}} - Кабинет
@endsection

@section('content')

<div class="container">
    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success">
                {!! Session::get('message') !!}
            </div>
        @endif
    </div>
</div>
@endsection
