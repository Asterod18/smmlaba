@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">

            <div class="panel-heading">Services</div>

            <div class="panel-body">

                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Название</th>
                      <th>Цена</th>
                      <th>Ключ</th>
                      <th>Категория</th>
                      <th>Действие</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($services as $service)
                      <tr>
                        <td>{{$service->id}}</td>
                          <td><a href="{{route('services.show',['id'=>$service->id])}}">{{$service->title}}</a></td>
                        <td>{{$service->price}}</td>
                        <td>{{$service->key}}</td>
                        <td>{{$service->Category->title}}</td>
                        <td>
                          <a href="{{route('services.edit',['id'=>$service->id])}}" class="btn btn-warning btn-xs">Edit</a>
                          {{ Form::open(['method' => 'DELETE', 'route' => ['services.destroy', $service->id]]) }}
                              {{ Form::hidden('id', $service->id) }}
                              {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
                          {{ Form::close() }}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                {{ $services->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
