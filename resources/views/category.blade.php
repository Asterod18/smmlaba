@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}">Главная</a></li>
                    @if($category->parent)
                        <li><a href="{{route('clientCategory',['id'=>$category->parent->id])}}">{{$category->parent->title}}</a></li>
                    @endif
                    <li class="active">{{$category->title}}</li>
                </ol>
                <div class="row Services">
                    @foreach($services as $service)
                        <div class="col-md-3 col-sm-6">
                            <a href="{{route('clientService',['slug'=>$service->key])}}">
                            <div class="service">
                                <div class="service_img"><img src="/images/services/{{$service->images}}"></div>
                                <div class="service_title">{{$service->title}}</div>
                            </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="description_text">
                    {!! $category->description !!}
                </div>
            </div>
        </div>
    </div>
@endsection
